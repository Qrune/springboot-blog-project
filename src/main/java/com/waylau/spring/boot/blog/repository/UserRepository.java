package com.waylau.spring.boot.blog.repository;

import com.waylau.spring.boot.blog.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
}
